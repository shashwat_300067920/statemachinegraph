# README #

### What is this repository for? ###

* Visualize spring state machines using graphviz-java

### How do I get set up? ###

* Install the maven packages as you usually do, all the dependencies are in the pom.xml

### Structure ###

* StateMachineGraph.java is the only file where actual work happens.
* Remaining files are simply there as examples for creating some of the state machines.

### Parameters of generateGraph ###
There are 3 parameters that generate graph function takes-

* stateMachine - the spring state machine
* externalOnly - whether you want to show the internal transitions or not
* sources - the initial state(s) from which the SM starts (can be an empty list)
* sinks - the terminal state(s) where SM ends (can be an empty list)