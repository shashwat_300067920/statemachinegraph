package com.myntra;

import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

import static com.myntra.logistics.platform.domain.ShipmentStatus.CANCELLATION_ALLOWED_FOR_FIRST_MILE_PICKUP;
import static com.myntra.logistics.platform.domain.ShipmentStatus.EXCHANGE_ON_HOLD;
import static com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.FIRST_MILE_STATUS_ALLOWED_FOR_INSCAN;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LOST_IN_HUB;
import static com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_IN_TRANSIT;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RECEIVED_IN_DISPATCH_HUB;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_COURIER_UPDATE;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_EXCHANGE_APPROVE;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_LOST_IN_HUB_EVENT_FOR_LMDO;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_RECEIVING_RTO_IN_HUB;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_RFID_RECEIVE_RTO;

public class AbstractHubStateTransitionConfiguration {


    private static ShipmentStatus[] allowedStatusForCancellation = new ShipmentStatus[]{
            ShipmentStatus.INSCANNED, ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, ShipmentStatus.SHIPPED, FAILED_DELIVERY};

    private static ShipmentUpdateEvent[] cancelledToInternalTransitionEvents = new ShipmentUpdateEvent[]{
            ShipmentUpdateEvent.CANCEL, ShipmentUpdateEvent.ADD_TO_MASTERBAG, ShipmentUpdateEvent.LOGISTICS_CANCELLATION};


    public void configure(StateMachineStateConfigurer<ShipmentStatus, ShipmentUpdateEvent> states) throws Exception {
        states.withStates()
                .initial(ShipmentStatus.PACKED)
                .choice(ShipmentStatus.PSEUDO_RECEIVED_IN_HUB_CHOICE_STATUS)
                .choice(ShipmentStatus.PSEUDO_PICKUP_IN_TRANSIT_CHOICE_STATUS)
                .choice(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS)
                .choice(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT)
                .choice(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)
                .states(EnumSet.allOf(ShipmentStatus.class));

//        .initial(ShipmentStatus.SHIPPED)
//                        .choice(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)
//                        .states(EnumSet.allOf(ShipmentStatus.class));
    }


    public void configure(StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {
        forwardFlowStateTransitions(transitions);
        cancelledShipmentsStateTransitions(transitions);
        rtoStateTransitions(transitions);
        firstMilePickupStateTransitions(transitions);
        lastMileDropOffTransitions(transitions);
        //toLostStateTransitions(transitions);
    }

    private void firstMilePickupStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {

        transitions.withExternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .target(ShipmentStatus.PSEUDO_RECEIVED_IN_HUB_CHOICE_STATUS)
                .and().withExternal()
                .source(PICKUP_IN_TRANSIT)
                .event(ShipmentUpdateEvent.SHIPPED)
                .target(PICKUP_IN_TRANSIT)
                .and().withInternal()
                .source(PICKUP_IN_TRANSIT)
                .event(ShipmentUpdateEvent.SHIPPED)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(PICKUP_IN_TRANSIT)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withExternal()
                .source(PICKUP_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .target(ShipmentStatus.PSEUDO_PICKUP_IN_TRANSIT_CHOICE_STATUS)
//                .and().withExternal()
//                .source(RECEIVED_IN_DISPATCH_HUB)
//                .event(ShipmentUpdateEvent.LOST_IN_HUB)
//                .target(LOST_IN_HUB)
                .and().withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.PACKED_ERROR)
                .target(ShipmentStatus.PACKED_ERROR)
                .and().withInternal()
                .source(ShipmentStatus.PACKED_ERROR)
                .event(ShipmentUpdateEvent.PACKED_ERROR);

        transitions.withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB);

        for (ShipmentStatus shipmentStatus : CANCELLATION_ALLOWED_FOR_FIRST_MILE_PICKUP) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.CANCEL)
                    .target(ShipmentStatus.CANCELLED);
        }

        for (ShipmentStatus shipmentStatus : CANCELLATION_ALLOWED_FOR_FIRST_MILE_PICKUP) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOGISTICS_CANCELLATION)
                    .target(ShipmentStatus.CANCELLED);
        }

        for (ShipmentStatus shipmentStatus : FIRST_MILE_STATUS_ALLOWED_FOR_INSCAN
        ) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.INSCAN)
                    .target(ShipmentStatus.INSCANNED)
                    .and().withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.SORT)
                    .target(ShipmentStatus.INSCANNED);
        }

        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_RECEIVED_IN_HUB_CHOICE_STATUS)
                .first(RECEIVED_IN_DISPATCH_HUB, ChoiceTransitionGuard.getInstance())
                .last(ShipmentStatus.PICKUP_DONE)
                .and().withChoice()
                .source(ShipmentStatus.PSEUDO_PICKUP_IN_TRANSIT_CHOICE_STATUS)
                .first(RECEIVED_IN_DISPATCH_HUB, ChoiceTransitionGuard.getInstance())
                .last(PICKUP_IN_TRANSIT);

        for (ShipmentStatus shipmentStatus : ShipmentStatus.APPROVED_ALLOWED_FOR_FIRST_MILE_PICKUP) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.APPROVE);
        }
    }

    private void forwardFlowStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.INSCAN)
                .target(ShipmentStatus.INSCANNED)
                .and().withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.SORT)
                .target(ShipmentStatus.INSCANNED)
                .and().withExternal()
                .source(ShipmentStatus.INSCANNED)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .target(ShipmentStatus.ADDED_TO_MB)
                .and().withExternal()
                .source(ShipmentStatus.INSCANNED)
                .event(ShipmentUpdateEvent.REASSIGN_COURIER)
                .target(ShipmentStatus.PACKED)
                .and().withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.SHIPPED)
                .target(ShipmentStatus.SHIPPED)
                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .target(ShipmentStatus.INSCANNED);


        transitions.withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.SHIPPED_WITHOUT_NOTIFICATION)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(ShipmentStatus.INSCANNED)
                .event(ShipmentUpdateEvent.SHIPPED_WITHOUT_NOTIFICATION)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.SHIPPED_WITHOUT_NOTIFICATION)
                .target(ShipmentStatus.SHIPPED);

        transitions.withInternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.SHIPMENT_DETAILS_CREATED)

                .and().withInternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.REASSIGN_COURIER)
                .and().withInternal()
                .source(ShipmentStatus.INSCANNED)
                .event(ShipmentUpdateEvent.INSCAN)
                .and().withInternal()
                .source(ShipmentStatus.INSCANNED)
                .event(ShipmentUpdateEvent.SORT)
                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.SHIPPED)
                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.SHIPPED_WITHOUT_NOTIFICATION)
                .and().withInternal()
                .source(ShipmentStatus.RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.FIRST_MILE_PICKUP_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_CREATED)
                .event(ShipmentUpdateEvent.FIRST_MILE_PICKUP_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.LAST_MILE_DROP_OFF_DETAILS_UPDATED);
        transitions.withExternal()
                .source(EXCHANGE_ON_HOLD)
                .event(ShipmentUpdateEvent.APPROVE)
                .target(FAILED_DELIVERY);


        // To Be Deleted
        transitions.withExternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.RECEIVE)
                .target(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS);

        transitions.withExternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .target(ShipmentStatus.RTO_IN_TRANSIT);

        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_COURIER_UPDATE) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.SHIPMENT_DETAILS_UPDATED);
        }

//        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_SORT) {
//            transitions.withInternal()
//                    .source(shipmentStatus)
//                    .event(ShipmentUpdateEvent.SORT);
//        }

        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_EXCHANGE_APPROVE) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.APPROVE);
        }
    }

    private void toLostStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        ShipmentStatus[] nonTerminalForwardFlowStatus = new ShipmentStatus[]{
                ShipmentStatus.INSCANNED,
                ShipmentStatus.ADDED_TO_MB, ShipmentStatus.PACKED, ShipmentStatus.PACKED_ERROR};
        for (ShipmentStatus shipmentStatus : nonTerminalForwardFlowStatus) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOST_IN_HUB)
                    .target(LOST_IN_HUB);
        }

        transitions.withExternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.LOST_IN_HUB)
                .target(ShipmentStatus.CANCELLED_LOST);

        ShipmentStatus[] nonTerminalRTOStatus = new ShipmentStatus[]{
                ShipmentStatus.RTO_DISPATCHED,
                ShipmentStatus.RECEIVED_IN_RETURNS_HUB,
                ShipmentStatus.READY_FOR_PICKUP_BY_SELLER};
        for (ShipmentStatus shipmentStatus : nonTerminalRTOStatus) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOST_IN_HUB)
                    .target(ShipmentStatus.LOST_IN_RETURNS_HUB);
        }
    }

    private void cancelledShipmentsStateTransitions(StateMachineTransitionConfigurer<ShipmentStatus,
            ShipmentUpdateEvent> transitions) throws Exception {

        for (ShipmentStatus shipmentStatus : allowedStatusForCancellation) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.CANCEL)
                    .target(ShipmentStatus.CANCELLED);
        }

        for (ShipmentStatus shipmentStatus : allowedStatusForCancellation) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOGISTICS_CANCELLATION)
                    .target(ShipmentStatus.CANCELLED);
        }

        transitions.withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.CANCEL)
                .target(ShipmentStatus.CANCELLED);

        transitions.withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.LOGISTICS_CANCELLATION)
                .target(ShipmentStatus.CANCELLED);

        transitions.withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.INSCAN)

                .and().withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.SORT);


        transitions.withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.CANCEL)
                .target(ShipmentStatus.CANCELLED);

        transitions.withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.LOGISTICS_CANCELLATION)
                .target(ShipmentStatus.CANCELLED);

        transitions.withExternal()
                .source(ShipmentStatus.PACKED_ERROR)
                .event(ShipmentUpdateEvent.CANCEL)
                .target(ShipmentStatus.CANCELLED);

        transitions.withExternal()
                .source(ShipmentStatus.PACKED_ERROR)
                .event(ShipmentUpdateEvent.LOGISTICS_CANCELLATION)
                .target(ShipmentStatus.CANCELLED);


        for (ShipmentUpdateEvent shipmentUpdateEvent : cancelledToInternalTransitionEvents) {
            transitions.withInternal()
                    .source(ShipmentStatus.CANCELLED)
                    .event(shipmentUpdateEvent);
        }

        transitions.withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.SHIPPED)
                .and().withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB);

        transitions.withExternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.RECEIVE)
                .target(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS);
    }

    private void rtoStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {

        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_RECEIVING_RTO_IN_HUB) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.RECEIVE)
                    .target(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS);
        }

        transitions.withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.RECEIVE)
                .target(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS);

        transitions.withInternal()
                .source(ShipmentStatus.RECEIVED_IN_RETURNS_HUB)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
//				.source(ShipmentStatus.RECEIVED_IN_RETURNS_HUB)
//				.event(ShipmentUpdateEvent.RECEIVE)
//		.and().withInternal()
                .source(ShipmentStatus.RTO_RECEIVED)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_SELLER)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_SELLER)
                .event(ShipmentUpdateEvent.DELIVERED_TO_SELLER)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_VENDOR)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .and().withInternal()
                .source(ShipmentStatus.RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .and().withInternal()
                .source(ShipmentStatus.RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .and().withInternal()
                .source(ShipmentStatus.RTO_DISPATCHED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .and().withInternal()
                .source(ShipmentStatus.FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB);

        transitions.withExternal()
                .source(ShipmentStatus.RECEIVED_IN_RETURNS_HUB)
                .event(ShipmentUpdateEvent.READY_FOR_PICKUP)
                .target(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .and().withExternal()
                .source(ShipmentStatus.RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RTO_DISPATCHED)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_IN_TRANSIT)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.DELIVERED_TO_SELLER)
                .target(ShipmentStatus.DELIVERED_TO_SELLER);


        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_RTO_RECEIVED_CHOICE_STATUS)
                .first(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED, ChoiceTransitionGuard.getInstance())
                .then(ShipmentStatus.DELIVERED_TO_SELLER, ChoiceTransitionGuard.getInstance())
                .then(ShipmentStatus.DELIVERED_TO_SELLER, ChoiceTransitionGuard.getInstance())
                .last(ShipmentStatus.RECEIVED_IN_RETURNS_HUB);
    }

    private void lastMileDropOffTransitions(StateMachineTransitionConfigurer<ShipmentStatus,
            ShipmentUpdateEvent> transitions) throws Exception {

//        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_LOST_IN_HUB_EVENT_FOR_LMDO) {
//            transitions.withExternal()
//                    .source(shipmentStatus)
//                    .event(ShipmentUpdateEvent.LOST_IN_HUB)
//                    .target(ShipmentStatus.RTO_LOST_IN_HUB);
//        }

        transitions.withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.SHIPPED)
                .and().withExternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .target(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT);

        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT)
                .first(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB, ChoiceTransitionGuard.getInstance())
                .last(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED);
        // TODO : RECEIVE event is sent twice by rejoy in testing.
        transitions.withInternal()
                .source(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB)
                .event(ShipmentUpdateEvent.RECEIVE);
        transitions.withInternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.RECEIVE);

        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_RFID_RECEIVE_RTO) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LAST_MILE_DROPOFF_RECEIVE);
        }

    }

}
