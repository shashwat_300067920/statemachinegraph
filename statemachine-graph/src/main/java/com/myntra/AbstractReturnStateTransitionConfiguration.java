package com.myntra;

import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

import static com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED_TO_SELLER;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_CREATED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_FAILED_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_RECEIVING_RETURN;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_RFID_RECEIVE_RETURN;

/**
 * Created by Abhinav on 12/02/16.
 */
public class AbstractReturnStateTransitionConfiguration {


    void configure(StateMachineStateConfigurer<ShipmentStatus, ShipmentUpdateEvent> states) throws Exception{
        states.withStates()
                .initial(ShipmentStatus.RETURN_CREATED)
                .choice(ShipmentStatus.PSEUDO_RETURN_RECEIVED_CHOICE_STATUS)
                .choice(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT)
                .states(EnumSet.allOf(ShipmentStatus.class));
    }


    void configure(StateMachineTransitionConfigurer<ShipmentStatus,ShipmentUpdateEvent> transitions) throws Exception{
        hubTransitions(transitions);
        //lostShipmentsStateTransitions(transitions);
        approveGreenChannel(transitions);
        lastMileDropOffStateTransitions(transitions);
        pickupStateTransition(transitions);
        logisticsAdminPrivilegedTransitions(transitions);
        financePrivilegedTransitions(transitions);
        ccPrivilegedTransitions(transitions);
    }


    private void hubTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {

        transitions.withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .target(ShipmentStatus.PICKUP_DONE)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .target(ShipmentStatus.RETURN_REJECTED)

                .and().withExternal()
                .source(ShipmentStatus.RECEIVED_IN_RETURNS_HUB)
                .event(ShipmentUpdateEvent.READY_FOR_PICKUP)
                .target(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .and().withExternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.DELIVERED_TO_SELLER)
                .target(ShipmentStatus.DELIVERED_TO_SELLER)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_PLATFORM)
                .event(ShipmentUpdateEvent.RETURN_REJECTED_BY_CC)
                .target(ShipmentStatus.RETURN_REJECTED)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_PLATFORM)
                .event(ShipmentUpdateEvent.RETURN_APPROVED_BY_CC)
                .target(ShipmentStatus.RETURN_CREATED)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.RETURN_IN_TRANSIT)
                .target(ShipmentStatus.RETURN_IN_TRANSIT)
                .and().withExternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_IN_TRANSIT)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .target(ShipmentStatus.DELIVERED_TO_VENDOR)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.CANCEL)
                .target(ShipmentStatus.CANCELLED);

        for(ShipmentStatus shipmentStatus: STATUSES_ALLOWED_FOR_RECEIVING_RETURN){
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.RECEIVE)
                    .target(ShipmentStatus.PSEUDO_RETURN_RECEIVED_CHOICE_STATUS);

        }

        transitions.withInternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_CREATED)

                .and().withInternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)

                .and().withInternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.RETURN_REJECTED)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .and().withInternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.RETURN_SUCCESSFUL)
                .and().withInternal()
                .source(ShipmentStatus.RETURN_REJECTED)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .and().withInternal()
                .source(ShipmentStatus.RETURN_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RETURN_IN_TRANSIT)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_RETURNS_HUB)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_SELLER)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_SELLER)
                .event(ShipmentUpdateEvent.DELIVERED_TO_SELLER)
                .and().withInternal()
                .source(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER)
                .event(ShipmentUpdateEvent.READY_FOR_PICKUP)

                .and().withInternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_APPROVED_BY_CC)

                .and().withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_PLATFORM)
                .event(ShipmentUpdateEvent.RETURN_ON_HOLD)
                .and().withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_PLATFORM)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.DELIVERED_TO_VENDOR)
                .event(ShipmentUpdateEvent.DELIVERED_TO_VENDOR)
                .and().withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.CANCEL);

        for(ShipmentStatus shipmentStatus: STATUSES_ALLOWED_FOR_RECEIVING_RETURN){
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.RECEIVE_IN_HUB);

        }

        for (ShipmentStatus shipmentStatus : ShipmentStatus.values()) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.NOT_DEFINED);
        }

        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_RETURN_RECEIVED_CHOICE_STATUS)
                .first(ShipmentStatus.ALTERATION_RETURN_RECEIVED, null)
                .then(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED, null)
                .then(ShipmentStatus.DELIVERED_TO_SELLER, null)
                .last(ShipmentStatus.READY_FOR_PICKUP_BY_SELLER);
    }

    private void approveGreenChannel(StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception{
        transitions.withInternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.APPROVE_GREEN_CHANNEL)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.APPROVE_GREEN_CHANNEL)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_AT_RETURNS_PROCESSING_CENTER)
                .event(ShipmentUpdateEvent.APPROVE_GREEN_CHANNEL)
                .and().withInternal()
                .source(ShipmentStatus.OUT_FOR_PICKUP)
                .event(ShipmentUpdateEvent.APPROVE_GREEN_CHANNEL);
    }

    private void lastMileDropOffStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {




        transitions.withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.RECEIVE)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.ADD_TO_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.REMOVE_FROM_MASTERBAG)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.SHIPPED)
                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB)
                .event(ShipmentUpdateEvent.RECEIVE);

        transitions.withInternal()
                .source(LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.LAST_MILE_DROP_OFF_MANIFEST)
                .and().withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.LAST_MILE_DROP_OFF_DETAILS_UPDATED);

        transitions.withExternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_HUB)
                .target(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT);
        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_RECEIVE_LMDO_SHIPMENT)
                .first(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB, null)
                .last(LAST_MILE_DROP_OFF_CREATED);
        transitions.withExternal()
                .source(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED_TO_SELLER)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED_TO_SELLER);

        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_RFID_RECEIVE_RETURN) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LAST_MILE_DROPOFF_RECEIVE);
        }
    }

    private void pickupStateTransition(StateMachineTransitionConfigurer<ShipmentStatus,ShipmentUpdateEvent> transitions) throws Exception{

        transitions.withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_ON_HOLD)
                .target(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.RETURN_SUCCESSFUL)
                .target(ShipmentStatus.RETURN_SUCCESSFUL)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.RETURN_ON_HOLD)
                .target(ShipmentStatus.ONHOLD_RETURN_WITH_COURIER)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.RETURN_SUCCESSFUL)
                .target(ShipmentStatus.RETURN_SUCCESSFUL)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .target(ShipmentStatus.RETURN_REJECTED)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.RETURN_IN_TRANSIT)
                .target(ShipmentStatus.RETURN_IN_TRANSIT)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_REJECTED)
                .event(ShipmentUpdateEvent.RESHIP_TO_CUSTOMER)
                .target(ShipmentStatus.RESHIPPED_DELIVERED_TO_CUSTOMER)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .target(ShipmentStatus.RETURN_REJECTED);




        transitions.withExternal()
                .source(ShipmentStatus.RETURN_CREATED)
                .event(ShipmentUpdateEvent.DOORSTEP_QC_COMPLETE)
                .target(ShipmentStatus.PICKUP_DONE_DOORSTEP_QC_COMPLETE)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_DONE_DOORSTEP_QC_COMPLETE)
                .event(ShipmentUpdateEvent.RETURN_SUCCESSFUL)
                .target(ShipmentStatus.RETURN_SUCCESSFUL);

        transitions.withInternal()
                .source(ShipmentStatus.PICKUP_DONE_DOORSTEP_QC_COMPLETE)
                .event(ShipmentUpdateEvent.DOORSTEP_QC_COMPLETE)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_DONE_DOORSTEP_QC_COMPLETE)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED);
    }

    private void logisticsAdminPrivilegedTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {

        transitions.withExternal()
                .source(ShipmentStatus.RETURN_REJECTED)
                .event(ShipmentUpdateEvent.RETURN_SUCCESSFUL)
                .target(ShipmentStatus.RETURN_SUCCESSFUL);
    }

    private void financePrivilegedTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {

        transitions.withExternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .target(ShipmentStatus.RETURN_REJECTED)
                .and().withExternal()
                .source(ShipmentStatus.RETURN_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RETURN_REJECTED)
                .target(ShipmentStatus.RETURN_REJECTED);
    }

    private void ccPrivilegedTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception{

        transitions.withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .event(ShipmentUpdateEvent.RETURN_REJECTED_BY_CC)
                .target(ShipmentStatus.RETURN_REJECTED)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .event(ShipmentUpdateEvent.RETURN_APPROVED_BY_CC)
                .target(ShipmentStatus.RETURN_CREATED)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_COURIER)
                .event(ShipmentUpdateEvent.RETURN_APPROVED_BY_CC)
                .target(ShipmentStatus.RETURN_SUCCESSFUL)
                .and().withExternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_COURIER)
                .event(ShipmentUpdateEvent.RETURN_REJECTED_BY_CC)
                .target(ShipmentStatus.RETURN_REJECTED);

        transitions.withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_COURIER)
                .event(ShipmentUpdateEvent.RETURN_ON_HOLD)
                .and().withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .event(ShipmentUpdateEvent.RETURN_ON_HOLD)
                .and().withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_CUSTOMER)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.ONHOLD_RETURN_WITH_COURIER)
                .event(ShipmentUpdateEvent.RETURN_DETAILS_UPDATED)
                .and().withInternal()
                .source(ShipmentStatus.RETURN_SUCCESSFUL)
                .event(ShipmentUpdateEvent.RETURN_CREATED);
    }

}
