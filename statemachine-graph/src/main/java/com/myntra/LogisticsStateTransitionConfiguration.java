package com.myntra;

import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.myntra.logistics.platform.domain.ShipmentStatus.DELAYED_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.DELIVERED_TO_SELLER;
import static com.myntra.logistics.platform.domain.ShipmentStatus.EXCHANGE_ON_HOLD;
import static com.myntra.logistics.platform.domain.ShipmentStatus.FAILED_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_CREATED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_FAILED_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LOST;
import static com.myntra.logistics.platform.domain.ShipmentStatus.LOST_ALLOWED_FOR_FIRST_MILE_PICKUP;
import static com.myntra.logistics.platform.domain.ShipmentStatus.OUT_FOR_DELIVERY;
import static com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_DONE;
import static com.myntra.logistics.platform.domain.ShipmentStatus.PICKUP_IN_TRANSIT;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RTO_CONFIRMED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RTO_DISPATCHED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RTO_INITIATED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RTO_IN_TRANSIT;
import static com.myntra.logistics.platform.domain.ShipmentStatus.RTO_LOST;
import static com.myntra.logistics.platform.domain.ShipmentStatus.SHIPPED;
import static com.myntra.logistics.platform.domain.ShipmentStatus.STATUSES_ALLOWED_FOR_LOST_EVENT_FOR_LMDO;

public class LogisticsStateTransitionConfiguration {

    private static final Set<ShipmentStatus> allowNdrStatusUpdate = new HashSet<>(Arrays.asList(
            RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER,
            OUT_FOR_DELIVERY,
            FAILED_DELIVERY,
            DELIVERED,
            DELAYED_DELIVERY,
            RTO_INITIATED,
            RTO_CONFIRMED,
            RTO_IN_TRANSIT,
            RTO_DISPATCHED,
            RTO_LOST,
            LOST,
            RECEIVED_IN_REGIONAL_HANDOVER_CENTER));

    private static final Set<ShipmentUpdateEvent> cancelledToInternalTransitionEvents =
            new HashSet<>(Arrays.asList(ShipmentUpdateEvent.IN_TRANSIT,ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL));


    public void configure(StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions) throws Exception {
        firstMileTransitions(transitions);
        firstMilePickupTransitions(transitions);
        forwardFlowStateTransitions(transitions);
        rtoStateTransitions(transitions);
        //toLostStateTransitions(transitions);
        noStateChangeTransitions(transitions);
        financePrevilegedTransitions(transitions);
        logisticsAdminPrevilegedTransitions(transitions);
        lastMileDropOffTransitions(transitions);
    }

    private void firstMilePickupTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception{
        transitions.withExternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.FIRST_MILE_PICKUP_MANIFEST)
                .target(ShipmentStatus.PICKUP_CREATED)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_CREATED)
                .event(ShipmentUpdateEvent.OUT_FOR_PICKUP)
                .target(ShipmentStatus.OUT_FOR_PICKUP)
                .and().withExternal()
                .source(ShipmentStatus.OUT_FOR_PICKUP)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .target(ShipmentStatus.PICKUP_DONE)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_CREATED)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .target(ShipmentStatus.PICKUP_DONE)
                .and().withExternal()
                .source(PICKUP_DONE)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)
                .target(PICKUP_IN_TRANSIT)
                .and().withExternal()
                .source(ShipmentStatus.OUT_FOR_PICKUP)
                .event(ShipmentUpdateEvent.FAILED_PICKUP)
                .target(ShipmentStatus.FAILED_PICKUP)
                .and().withExternal()
                .source(ShipmentStatus.FAILED_PICKUP)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .target(ShipmentStatus.PICKUP_DONE)
                .and().withExternal()
                .source(ShipmentStatus.FAILED_PICKUP)
                .event(ShipmentUpdateEvent.OUT_FOR_PICKUP)
                .target(ShipmentStatus.OUT_FOR_PICKUP)
                .and().withExternal()
                .source(ShipmentStatus.PICKUP_DONE)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(ShipmentStatus.PICKUP_IN_TRANSIT)
                .and().withInternal()
                .source(PICKUP_IN_TRANSIT)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .and().withInternal()
                .source(ShipmentStatus.PICKUP_CREATED)
                .event(ShipmentUpdateEvent.FIRST_MILE_PICKUP_MANIFEST);

//        for(ShipmentStatus shipmentStatus : LOST_ALLOWED_FOR_FIRST_MILE_PICKUP){
//            transitions.withExternal()
//                    .source(shipmentStatus)
//                    .event(ShipmentUpdateEvent.LOST)
//                    .target(ShipmentStatus.LOST);
//
//
//
//        }
    }

    private void firstMileTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withInternal()
                .source(ShipmentStatus.PACKED)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .and().withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.PICKUP_DONE)
                .and().withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.PICKUP_IN_TRANSIT);

//        transitions.withExternal()
//                .source(ShipmentStatus.PACKED)
//                .event(ShipmentUpdateEvent.LOST)
//                .target(LOST);
    }

    private void forwardFlowStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {

        transitions.withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)



                .and().withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)




                .and().withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_FORWARD_HANDOVER_CENTER)
                .target(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)




                .and().withExternal()
                .source(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(ShipmentStatus.SHIPPED)

        ;

        for (ShipmentUpdateEvent shipmentUpdateEvent : cancelledToInternalTransitionEvents){
            transitions.withInternal()
                    .source(ShipmentStatus.CANCELLED)
                    .event(shipmentUpdateEvent)

            ;
        }

        transitions.withInternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_FORWARD_HANDOVER_CENTER);

        transitions.withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(OUT_FOR_DELIVERY)

                .and().withExternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)



                .and().withExternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY)


                .and().withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY)


                .and().withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(OUT_FOR_DELIVERY)


                .and().withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)




                .and().withExternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)




                .and().withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)



        ;

        transitions.withExternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.RTO_INITIATED)
                .target(RTO_INITIATED)
                .and().withExternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.RTO_INITIATED)
                .target(RTO_INITIATED)
                .and().withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.RTO_INITIATED)
                .target(RTO_INITIATED);

        transitions.withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)
                .and().withExternal()
                .source(ShipmentStatus.ADDED_TO_MB)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY);

        transitions.withChoice()
                .source(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)
                .first(DELAYED_DELIVERY, ChoiceTransitionGuard.getInstance())
                .last(DELIVERED);
    }

    private void rtoStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)
                .and().withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.RTO_CANCELLED)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)
                .target(ShipmentStatus.SHIPPED)
                .and().withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED)
                .and().withExternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(OUT_FOR_DELIVERY);
        transitions.withExternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.ON_HOLD)
                .target(ShipmentStatus.EXCHANGE_ON_HOLD);

        transitions.withExternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.ON_HOLD)
                .target(ShipmentStatus.EXCHANGE_ON_HOLD)
                .and().withExternal()
                .source(EXCHANGE_ON_HOLD)
                .event(ShipmentUpdateEvent.EXCHANGE_REJECTED)
                .target(RTO_CONFIRMED);

        transitions.withExternal()
                .source(RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(RTO_IN_TRANSIT)
                .and().withExternal()
                .source(RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)
                .target(ShipmentStatus.RTO_IN_TRANSIT)

                .and().withExternal()
                .source(ShipmentStatus.RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.RTO_DISPATCHED)
                .target(RTO_DISPATCHED)
                .and().withExternal()
                .source(RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RTO_DISPATCHED)
                .target(RTO_DISPATCHED);


        transitions.withExternal()
                .source(RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_REVERSE_HANDOVER_CENTER)
                .target(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .and().withExternal()
                .source(RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.RECEIVE_IN_REVERSE_HANDOVER_CENTER)
                .target(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .and().withExternal()
                .source(RTO_DISPATCHED)
                .event(ShipmentUpdateEvent.RECEIVE_IN_REVERSE_HANDOVER_CENTER)
                .target(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .and().withExternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.IN_TRANSIT)
                .target(RTO_IN_TRANSIT)
                .and().withExternal()
                .source(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(ShipmentStatus.PSEUDO_DELIVERED_CHOICE_STATUS)
                .and().withExternal()
                .source(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)
                .and().withExternal()
                .and().withExternal()
                .source(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(OUT_FOR_DELIVERY)
                .and().withExternal()
                .source(RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY);
    }

    private void toLostStateTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        ShipmentStatus[] nonTerminalForwardFlowStatus = new ShipmentStatus[] {
                SHIPPED, RECEIVED_IN_FORWARD_REGIONAL_HANDOVER_CENTER, OUT_FOR_DELIVERY,
                FAILED_DELIVERY, RTO_INITIATED, EXCHANGE_ON_HOLD};
        for (ShipmentStatus shipmentStatus : nonTerminalForwardFlowStatus) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOST)
                    .target(LOST)

            ;
        }

        transitions.withExternal()
                .source(ShipmentStatus.CANCELLED)
                .event(ShipmentUpdateEvent.LOST)
                .target(ShipmentStatus.CANCELLED_LOST);

        transitions.withExternal()
                .source(ShipmentStatus.CANCELLED_LOST)
                .event(ShipmentUpdateEvent.LOST)
                .target(ShipmentStatus.CANCELLED_LOST);

        transitions.withExternal()
                .source(ShipmentStatus.CANCELLED_LOST)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(ShipmentStatus.CANCELLED);

        ShipmentStatus[] nonTerminalRTOStatus = new ShipmentStatus[] {
                RTO_CONFIRMED, RTO_IN_TRANSIT, ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER,
                RTO_DISPATCHED };
        for (ShipmentStatus shipmentStatus : nonTerminalRTOStatus) {
            transitions.withExternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.LOST)
                    .target(RTO_LOST)
            ;
        }
    }

    private void noStateChangeTransitions(StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.IN_TRANSIT)

                .and().withInternal()
                .source(RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.IN_TRANSIT)

                .and().withInternal()
                .source(RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)

                .and().withInternal()
                .source(ShipmentStatus.SHIPPED)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)
        ;
        // For Idempotency of event processing.
        transitions.withInternal()
                .source(OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
//                .and().withInternal()
//                .source(LOST)
//                .event(ShipmentUpdateEvent.LOST)
                .and().withInternal()
                .source(DELIVERED)
                .event(ShipmentUpdateEvent.DELIVERED)
                .and().withInternal()
                .source(DELAYED_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .and().withInternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.IN_TRANSIT_NON_CRITICAL)
                .and().withInternal()
                .source(FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .and().withInternal()
                .source(RTO_INITIATED)
                .event(ShipmentUpdateEvent.RTO_INITIATED)
                .and().withInternal()
                .source(RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .and().withInternal()
                .source(RTO_DISPATCHED)
                .event(ShipmentUpdateEvent.RTO_DISPATCHED)
                .and().withInternal()
                .source(DELIVERED)
                .event(ShipmentUpdateEvent.PAYMENT_RECEIVED)

                .and().withInternal()
                .source(DELAYED_DELIVERY)
                .event(ShipmentUpdateEvent.PAYMENT_RECEIVED)

                .and().withInternal()
                .source(ShipmentStatus.RECEIVED_IN_REGIONAL_HANDOVER_CENTER)
                .event(ShipmentUpdateEvent.RECEIVE_IN_REVERSE_HANDOVER_CENTER);




        for (ShipmentStatus shipmentStatus :allowNdrStatusUpdate) {
            transitions.withInternal()
                    .source(shipmentStatus)
                    .event(ShipmentUpdateEvent.NDR_UPDATE)
            ;
        }


//        for (ShipmentStatus shipmentStatus : ShipmentStatus.values()) {
//            transitions.withInternal()
//                    .source(shipmentStatus)
//                    .event(ShipmentUpdateEvent.NOT_DEFINED);
//        }
    }

    private void financePrevilegedTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withExternal()
                .source(RTO_IN_TRANSIT)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED)


                .and().withExternal()
                .source(RTO_CONFIRMED)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED)


                .and().withExternal()
                .source(RTO_DISPATCHED)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED);


//                .and().withExternal()
//                .source(LOST)
//                .event(ShipmentUpdateEvent.DELIVERED)
//                .target(DELIVERED)
//
//                .and().withExternal()
//                .source(ShipmentStatus.LOST_IN_HUB)
//                .event(ShipmentUpdateEvent.DELIVERED)
//                .target(DELIVERED)        ;
    }

    private void logisticsAdminPrevilegedTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception {
        transitions.withExternal()
//                .source(LOST)
//                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
//                .target(RTO_CONFIRMED)
//
//
//                .and().withExternal()
//                .source(ShipmentStatus.LOST_IN_HUB)
//                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
//                .target(RTO_CONFIRMED)


                .and().withExternal()
                .source(DELIVERED)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)





//                .and().withExternal()
//                .source(DELIVERED)
//                .event(ShipmentUpdateEvent.LOST)
//                .target(LOST)


                .and().withExternal()
                .source(DELIVERED)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY)




                .and().withExternal()
                .source(DELAYED_DELIVERY)
                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
                .target(RTO_CONFIRMED)




//                .and().withExternal()
//                .source(DELAYED_DELIVERY)
//                .event(ShipmentUpdateEvent.LOST)
//                .target(LOST)

                .and().withExternal()
                .source(DELAYED_DELIVERY)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(FAILED_DELIVERY);



//                .and().withExternal()
//                .source(RTO_LOST)
//                .event(ShipmentUpdateEvent.RTO_CONFIRMED)
//                .target(RTO_CONFIRMED);

    }

    private void lastMileDropOffTransitions(
            StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions)
            throws Exception{

//        for (ShipmentStatus shipmentStatus : STATUSES_ALLOWED_FOR_LOST_EVENT_FOR_LMDO) {
//            transitions.withExternal()
//                    .source(shipmentStatus)
//                    .event(ShipmentUpdateEvent.LOST)
//                    .target(RTO_LOST)
//            ;
//        }

        transitions.withInternal()
                .source(LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.LAST_MILE_DROP_OFF_MANIFEST);
        transitions.withExternal()
                .source(ShipmentStatus.RECEIVED_IN_LAST_MILE_DROPOFF_HANDOVER_HUB)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.FAILED_DELIVERY)
                .target(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.OUT_FOR_DELIVERY)
                .target(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_FAILED_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED_TO_SELLER)
                .and().withExternal()
                .source(LAST_MILE_DROP_OFF_OUT_FOR_DELIVERY)
                .event(ShipmentUpdateEvent.DELIVERED)
                .target(DELIVERED_TO_SELLER);

        transitions.withInternal()
                .source(ShipmentStatus.LAST_MILE_DROP_OFF_CREATED)
                .event(ShipmentUpdateEvent.IN_TRANSIT);
    }

}
