package com.myntra;

/**
 *  Heavily inspired by https://gist.github.com/wolframite/78afb9b6891fe2815252d300020731dd
 */

import guru.nidi.graphviz.attribute.Label;
import guru.nidi.graphviz.attribute.Rank;
import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.Graph;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.Node;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.ChoicePseudoState;
import org.springframework.statemachine.state.PseudoStateKind;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.transition.TransitionKind;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static guru.nidi.graphviz.model.Factory.mutGraph;
import static guru.nidi.graphviz.model.Factory.node;
import static guru.nidi.graphviz.model.Link.to;

public class StateMachineGraph<S, E> {

    public void generateGraph(StateMachine<S, E> stateMachine, boolean externalOnly, List<S> sources, List<S> sinks)
            throws IOException, NoSuchFieldException {
        MutableGraph graph = mutGraph("Order State Machine").setDirected(true).graphAttrs()
                .add(Rank.dir(Rank.RankDir.TOP_TO_BOTTOM));
        Map<String, Set<GraphVizLink>> graphMap = new HashMap<>();
        addStates(graphMap, stateMachine.getStates());
        addTransitions(graphMap, stateMachine.getTransitions(), externalOnly);
        addPseudoTransitions(graphMap, stateMachine.getStates());
        createGraph(graphMap, graph);
        addSourcesAndSinks(graph, sources, sinks);
        drawGraph(graph);
    }

    private void addSourcesAndSinks(MutableGraph graph, List<S> sources, List<S> sinks) {
        graph.add(getSubGraph(sinks, Rank.RankType.SINK));
        graph.add(getSubGraph(sources, Rank.RankType.SOURCE));
    }

    private void drawGraph(MutableGraph graph) throws IOException {
        File file = new File("return.png");
        Graphviz.fromGraph(graph)
                .scale(3.0)
                .render(Format.PNG)
                .toFile(file);
    }

    private void addStates(Map<String, Set<GraphVizLink>> graphMap, Collection<State<S, E>> states) {
        states.forEach(
                s -> graphMap.put(s.getId().toString(), new HashSet<>())
        );
    }

    private void addTransitions(Map<String, Set<GraphVizLink>> graphMap, Collection<Transition<S, E>> transitions, boolean externalOnly) {
        transitions.forEach(
                t -> {
                    if (t.getKind().equals(TransitionKind.INTERNAL) && externalOnly) return;
                    graphMap.get(t.getSource().getId().toString()).add(
                            new GraphVizLink(t.getTarget().getId().toString(), t.getTrigger().getEvent().toString()));
                }
        );
    }

    private void addPseudoTransitions(Map<String, Set<GraphVizLink>> graphMap, Collection<State<S, E>> states) throws NoSuchFieldException {
        Field choicesField = ChoicePseudoState.class.getDeclaredField("choices");
        choicesField.setAccessible(true);

        states.forEach(
                s -> {
                    if (Objects.nonNull(s.getPseudoState()) && s.getPseudoState().getKind().equals(PseudoStateKind.CHOICE)) {
                        final String source = s.getId().toString();
                        final ChoicePseudoState<S, E> pseudoState = (ChoicePseudoState<S, E>) s.getPseudoState();
                        try {
                            final Object o = choicesField.get(pseudoState);
                            List<ChoicePseudoState.ChoiceStateData<S, E>> choices = (List<ChoicePseudoState.ChoiceStateData<S, E>>) o;
                            choices.forEach(choice -> {
                                        graphMap.get(source).add(
                                                new GraphVizLink(choice.getState().getId().toString(), "ChoiceTransition"));
                                    }
                            );

                        } catch (IllegalAccessException ignored) {

                        }
                    }
                }
        );
    }

    private void createGraph(Map<String, Set<GraphVizLink>> graphMap, MutableGraph graph) {
        graphMap.forEach((key, targetSet) -> {
            Node node = getNode(key);
            targetSet.forEach(l -> graph.add(
                    node.link(to(node(l.getTarget())).with(Label.of(l.getLabel().toLowerCase())))
            ));
        });
    }

    private Graph getSubGraph(List<S> states, Rank.RankType rankType) {
        MutableGraph subgraph = Factory.graph(rankType.toString())
                .graphAttr().with(Rank.inSubgraph(rankType)).toMutable();
        states.forEach(
                state -> subgraph.add(node(state.toString()))
        );
        return subgraph.toImmutable();
    }

    private Node getNode(String key) {
        return node(key).with(Shape.RECTANGLE);
    }

}

class GraphVizLink {

    private final String target;
    private final String label;

    GraphVizLink(String target, String label) {
        this.target = target;
        this.label = label;
    }

    public String getTarget() {
        return target;
    }

    public String getLabel() {
        return label;
    }
}
