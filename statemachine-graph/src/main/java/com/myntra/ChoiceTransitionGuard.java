package com.myntra;

import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

public class ChoiceTransitionGuard implements Guard<ShipmentStatus, ShipmentUpdateEvent> {

    public static ChoiceTransitionGuard getInstance() {
        return new ChoiceTransitionGuard();
    }

    @Override
    public boolean evaluate(StateContext<ShipmentStatus, ShipmentUpdateEvent> stateContext) {
        return true;
    }
}
