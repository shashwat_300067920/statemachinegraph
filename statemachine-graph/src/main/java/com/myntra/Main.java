package com.myntra;

import com.myntra.logistics.platform.domain.ShipmentStatus;
import com.myntra.logistics.platform.domain.ShipmentUpdateEvent;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.Arrays;
import java.util.Collections;


public class Main {

    public static void main(String[] args) throws Exception {

        StateMachineGraph<ShipmentStatus, ShipmentUpdateEvent> stateMachineGraph = new StateMachineGraph<>();
        stateMachineGraph.generateGraph(getStatemachine(), true,
                Collections.singletonList(ShipmentStatus.RETURN_CREATED),
                Arrays.asList(ShipmentStatus.DELIVERED_TO_SELLER, ShipmentStatus.DELIVERED_TO_VENDOR,
                        ShipmentStatus.ALTERATION_RETURN_RECEIVED, ShipmentStatus.RESHIPPED_DELIVERED_TO_CUSTOMER));

    }

    private static StateMachine<ShipmentStatus, ShipmentUpdateEvent> getStatemachine() throws Exception {

//        AbstractHubStateTransitionConfiguration abstractHubStateTransitionConfiguration =
//                new AbstractHubStateTransitionConfiguration();
//        LogisticsStateTransitionConfiguration logisticsStateTransitionConfiguration =
//                new LogisticsStateTransitionConfiguration();

        AbstractReturnStateTransitionConfiguration abstractReturnStateTransitionConfiguration =
            new AbstractReturnStateTransitionConfiguration();

        StateMachineBuilder.Builder<ShipmentStatus, ShipmentUpdateEvent> builder = StateMachineBuilder.builder();
        StateMachineStateConfigurer<ShipmentStatus, ShipmentUpdateEvent> states = builder.configureStates();
        StateMachineTransitionConfigurer<ShipmentStatus, ShipmentUpdateEvent> transitions = builder.configureTransitions();

//        abstractHubStateTransitionConfiguration.configure(states);
//        abstractHubStateTransitionConfiguration.configure(transitions);
//        logisticsStateTransitionConfiguration.configure(transitions);

        abstractReturnStateTransitionConfiguration.configure(states);
        abstractReturnStateTransitionConfiguration.configure(transitions);

        return builder.build();

    }


}
